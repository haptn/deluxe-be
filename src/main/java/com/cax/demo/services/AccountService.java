package com.cax.demo.services;

import java.util.Collection;

import com.cax.demo.models.Account;
import com.cax.demo.models.Role;

public interface AccountService {
	Account saveAccount(Account account);
	Role saveRole(Role role);
	void addRoleToUser(String username, String roleName);
	Account getAccount(String username);
	Collection<Account> getAccounts();
	void updateRoleToUser(String username, String roleName);
	void saveToken(String username,String token);
	Account findByToken(String token);
}
