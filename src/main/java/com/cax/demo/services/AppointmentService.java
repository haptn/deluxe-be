package com.cax.demo.services;

import java.text.ParseException;
import java.util.Collection;

import com.cax.demo.dtos.AppointmentDTO;
import com.cax.demo.dtos.AppointmentFilter;
import com.cax.demo.dtos.AppointmentStatus;
import com.cax.demo.dtos.PagingationRequest;
import com.cax.demo.dtos.Pagingnation;
import com.cax.demo.models.Details;

public interface AppointmentService {
	boolean add(AppointmentDTO<Long> dto) throws ParseException;
	boolean updateStatus(Long id,String status);
	Pagingnation<AppointmentDTO<String>> getAll(PagingationRequest page,String key,
			String status, String start, String end,String guestName,Long[] serviceIds,Long[] technicianIds);
	Pagingnation<AppointmentDTO<String>> getExample(AppointmentFilter filter,PagingationRequest page);
	long getPendingAppointment(AppointmentStatus status);
	Collection<String> getServiceName(Collection<Details> details);
}
