package com.cax.demo.services;

import java.text.ParseException;
import java.util.List;

import com.cax.demo.dtos.CustomerCheckinRequest;
import com.cax.demo.dtos.CustomerDTO;
import com.cax.demo.dtos.CustomerFilter;
import com.cax.demo.dtos.CustomerHistoryDTO;
import com.cax.demo.dtos.CutomerCheckOutRequest;
import com.cax.demo.dtos.Pagingnation;
import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;

public interface CustomerService {
	Pagingnation<CustomerDTO> getAll(CustomerFilter filter);

	Customer checkIn(CustomerCheckinRequest request, String userType) throws ParseException;

	Customer details(long id);

	List<CustomerHistoryDTO> getHistories(String phone);

	Customer update(Customer customer);

	CustomerDTO delete(String phone);

	CustomerHistory updateHistory(CustomerHistory request);

	CustomerHistoryDTO deleteHistory(Long id);

	Integer getIndex(String phone);

	CustomerDTO checkIndexRequestUnConfirmed(String phone);

	void confirm(Long id) throws ParseException, NullPointerException;

	CustomerDTO checkOut(CutomerCheckOutRequest request) throws ParseException, NullPointerException;

	String[] getNameByPhone(String phone);

	CustomerHistory detailsHistory(long id);

	List<String> getPhoneListByCheckinStatus(Integer limit);
}
