package com.cax.demo.services;

import java.util.Collection;

import com.cax.demo.exceptions.DuplicateKeyException;
import com.cax.demo.exceptions.NotFoundException;

public interface ServiceOptionService {
	Collection<?> getAllServices();
	void cleanCache();
	Collection<?> getAllServiceForAdmin();
	Collection<?> getMenuServices();
	String addNewFlavor(String flavor, long id) throws NotFoundException,DuplicateKeyException;
	String addFlavorToOption(long flavorId, long serviceId) throws NotFoundException,DuplicateKeyException;
	String removeOpionFlavor(long id) throws NotFoundException;
	String removeFlavor(long id) throws NotFoundException;	
}
