package com.cax.demo.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.CustomerImport;
import com.cax.demo.utils.DateTimeFormater;

public class CSVHelper {
	public static String TYPE = "text/csv";
	static String[] HEADERs = { "Id", "Title", "Description", "Published" };

	public static boolean hasCSVFormat(MultipartFile file) {
		if (!TYPE.equals(file.getContentType())) {
			return false;
		}
		return true;
	}

	public static List<CustomerImport> csvToTutorials(InputStream is) {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				CSVParser csvParser = new CSVParser(fileReader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withAllowMissingColumnNames());) {
			List<CustomerImport> tutorials = new ArrayList<CustomerImport>();
			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
			DateFormat formatter = new SimpleDateFormat("M/d/yy HH:mm");
			formatter.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
			for (CSVRecord csvRecord : csvRecords) {
				CustomerImport tutorial = CustomerImport.builder()
											.fullName(csvRecord.get("Full Name"))
											.phone(csvRecord.get("Phone"))
											.email(csvRecord.get("Email"))
											.lastVisited((Date)formatter.parse(csvRecord.get("Last Visited")))
											.birthDay(Integer.parseInt(csvRecord.get(5)))
											.birthMonth(Integer.parseInt(csvRecord.get(6)))
											.currentPoint(Double.parseDouble(csvRecord.get("Point")))
											.totalPoint(Double.parseDouble(csvRecord.get("Life Time Point")))
											.visitCount(Integer.parseInt(csvRecord.get("Visit Count")))
											.build();
				tutorial = tutorial.convertBirthdate().convertDisscount();
				tutorials.add(tutorial);
			}
			return tutorials;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		} catch (ParseException ex) {
			throw new RuntimeException("fail to parse CSV file: " + ex.getMessage());
		}
	}
}
