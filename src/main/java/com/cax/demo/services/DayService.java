package com.cax.demo.services;

import java.util.Collection;

import com.cax.demo.dtos.DayDTO;

public interface DayService {
	Collection<?> getSlot(String date);
	void save(DayDTO dto);
	void recover(String name);
}
