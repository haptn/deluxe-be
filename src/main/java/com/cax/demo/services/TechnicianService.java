package com.cax.demo.services;

import java.io.IOException;
import java.util.Collection;

import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.PhotoDTO;
public interface TechnicianService {
	Collection<?> getAll();
	void cleanCache();
	void saveImage(MultipartFile imageFile, PhotoDTO photoDTO) throws IOException;
}
