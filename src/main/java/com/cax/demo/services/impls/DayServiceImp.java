package com.cax.demo.services.impls;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cax.demo.dtos.DayDTO;
import com.cax.demo.dtos.Time;
import com.cax.demo.models.Day;
import com.cax.demo.models.Slot;
import com.cax.demo.repositories.DayRepository;
import com.cax.demo.repositories.SlotRepository;
import com.cax.demo.services.DayService;
import com.cax.demo.utils.DateTimeFormater;

@Service 
public class DayServiceImp implements DayService{
	
	@Autowired
	private DayRepository repo;

	@Autowired 
	private SlotRepository slotRepo;
	
	@Override
	public Collection<?> getSlot(String date){	
		Day day = repo.findByName(date);
		if(ObjectUtils.isEmpty(day)) {	
			String name = getDayOfWeek(date);
			if(!ObjectUtils.isEmpty(name)) {
			day = repo.findByName(getDayOfWeek(date));
			}
			if(ObjectUtils.isEmpty(day)||ObjectUtils.isEmpty(day.getSlots())) {
				day = repo.findByName("0");
			}
		}else {
			if(ObjectUtils.isEmpty(day.getSlots())) {
				String name = getDayOfWeek(date);
				if(!ObjectUtils.isEmpty(name)) {
				day = repo.findByName(getDayOfWeek(date));
				}
				day = repo.findByName("0");
			}
		}
		Collection<Time> slot = new ArrayList<>();
		for (Slot dateSlot : day.getSlots()) {
			String label = dateSlot.getLabel();
			long id = dateSlot.getId();
			int value =  dateSlot.getValue();
			slot.add(new Time(id,label,value));
		}
		return slot;
	}
	
	private String getDayOfWeek(String date) {
		Date a= null;
			try {
				a = DateTimeFormater.getFormatterOnlyDate().parse(date);
			} catch (ParseException e1) {
				return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(a);
		return String.valueOf(c.get(Calendar.DAY_OF_WEEK)) ;
	}
	
	public void save(DayDTO dto) {
		Day day = repo.findByName(dto.getName());
		Collection<Slot> result = new ArrayList<Slot>();
		for (Long slotId : dto.getSlotIds()) {			
			result.add(slotRepo.getById(slotId));
		}	
		if(ObjectUtils.isEmpty(day)) {	
				day = new Day(dto.getName(), null);	
				day.setSlots(result);	
		}else {
			day.setSlots(result);
		}
		repo.save(day);
	}

	@Override
	public void recover(String name) {
		Day day = repo.findByName(name);
		day.setSlots(null);
		try {
			Integer.parseInt(name);
			repo.save(day);
		} catch (Exception e) {
			repo.delete(day);
		}
	}	
}
