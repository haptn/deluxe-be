package com.cax.demo.services.impls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cax.demo.dtos.ServiceDTO;
import com.cax.demo.exceptions.DuplicateKeyException;
import com.cax.demo.exceptions.NotFoundException;
import com.cax.demo.models.Flavor;
import com.cax.demo.models.ServiceGroup;
import com.cax.demo.models.ServiceOption;
import com.cax.demo.models.Services;
import com.cax.demo.repositories.CustomSpecification;
import com.cax.demo.repositories.FlavorRepository;
import com.cax.demo.repositories.ServiceGroupRepository;
import com.cax.demo.repositories.ServiceOptionRepository;
import com.cax.demo.repositories.ServiceRepository;
import com.cax.demo.services.ServiceOptionService;


@Service
public class ServiceOptionsServiceImp implements ServiceOptionService{
	@Autowired
	private ServiceGroupRepository groupRepo;
	
	@Autowired
	private FlavorRepository flavorRepo;
	@Autowired
	private ServiceOptionRepository optionRepo;
	
	@Autowired
	private ServiceRepository serviceRepo;
	
	@Override
	@Cacheable("bookingServices")
	public Collection<?> getAllServices(){
		Collection<ServiceGroup> groupEntities = groupRepo.findAll();
		if(!ObjectUtils.isEmpty(groupEntities)) {
			List<ServiceDTO> groupItems = new ArrayList<>();
			for (ServiceGroup entity : groupEntities) {
				ServiceDTO menu = new ServiceDTO(entity.getId(),entity.getName(),null);
				if(!ObjectUtils.isEmpty(entity.getServices())) {
				List<ServiceDTO> serviceItems = new ArrayList<>();
				for (Services service : entity.getServices()) {
					if(service.getScope().equals("both")|| service.getScope().equals("booking")) {
					ServiceDTO serviceItem = new ServiceDTO(service.getId(), service.getName(), null, service.getImage(), service.getPrice(), service.isFixedPrice(), entity.getName(), service.getScope(),service.isActive(), service.getDescription(), service.getNote());					
					if(!ObjectUtils.isEmpty(service.getOptions())) {
						List<ServiceDTO> optionFlavors = new ArrayList<ServiceDTO>();
						for (ServiceOption option : service.getOptions()) {
							String name = service.getName();
							if(!ObjectUtils.isEmpty(option.getFlavor()))
								name = option.getFlavor().getName();
							ServiceDTO flavor = new ServiceDTO(option.getId(), name );
							optionFlavors.add(flavor);
						}
						serviceItem.setList(optionFlavors);
					}
					serviceItems.add(serviceItem);
				}
				}
				menu.setList(serviceItems);
				}
				groupItems.add(menu);
			}
			return groupItems;
		}
		return null;
	}
	
	@Override
	@CacheEvict(value = "menu", allEntries = true)
    public void cleanCache() {
    }

	@Override
	@Cacheable("adminServices")
	public Collection<?> getAllServiceForAdmin() {
		String[] array = {"both","booking","menu"};
		return getServices(array);
	}
	

	@Override
	public Collection<?> getMenuServices() {
		String[] array = {"both","menu"};
		return getServices(array);
	}
	private Collection<?> getServices(String[] array) {
		@SuppressWarnings("unchecked")
		Specification<Services> condition = (Specification<Services>) Specification.where( CustomSpecification.findInString(array, "scope"));
		List<Services> entity = serviceRepo.findAll(condition);
		Collection<ServiceDTO> serviceItems = new ArrayList<>();
		for (Services service : entity) {	
			ServiceDTO serviceItem = new ServiceDTO(service.getId(), service.getName(), null, service.getImage(), service.getPrice(), service.isFixedPrice(),service.getGroup().getName(), service.getScope(), service.isActive(), service.getDescription(), service.getNote());					
			if(!ObjectUtils.isEmpty(service.getOptions())) {
				List<ServiceDTO> optionFlavors = new ArrayList<ServiceDTO>();
				for (ServiceOption option : service.getOptions()) {
					String name = service.getName();
					if(!ObjectUtils.isEmpty(option.getFlavor()))
						name = option.getFlavor().getName();
					ServiceDTO flavor = new ServiceDTO(option.getId(), name );
					optionFlavors.add(flavor);
				}
				serviceItem.setList(optionFlavors);
			}
			serviceItems.add(serviceItem);
		}
		
		return serviceItems;
	}

	@Override
	public String addNewFlavor(String flavor, long id) throws NotFoundException, DuplicateKeyException {
		boolean isDuplicate = flavorRepo.existsFlavorByName(flavor);
		
		if(isDuplicate)
			throw new DuplicateKeyException("\""+flavor+"\""+" is duplicated");
		Optional<Services> service = serviceRepo.findById(id);
		if(ObjectUtils.isEmpty(service))
			throw new NotFoundException("The Service \""+id+"\""+" is not found");
		Flavor newFlavor = flavorRepo.save(new Flavor(flavor,null));
		ServiceOption option = new ServiceOption();
		option.setFlavor(newFlavor);
		option.setService(service.get());
		option = optionRepo.save(option);
		return ObjectUtils.isEmpty(option)?"ERROR" : "Passed";
	}

	@Override
	public String removeOpionFlavor(long id) throws NotFoundException {
		boolean exisited = optionRepo.existsById(id);
		if (!exisited)
			throw new NotFoundException("The flavor option ID \""+id +"\" not found");
		optionRepo.deleteById(id);
		return  "Deleted";
	}

	@Override
	public String removeFlavor(long id) throws NotFoundException {
		boolean exisited = optionRepo.existsById(id);
		if (!exisited)
			throw new NotFoundException("The flavor option ID \""+id +"\" not found");
		Optional<ServiceOption> option = optionRepo.findById(id);
		Flavor flavor = option.get().getFlavor();
		if(ObjectUtils.isEmpty(flavor))
			throw new NotFoundException("The flavor of option ID \""+id +"\" not found");
		flavorRepo.delete(flavor);
		return "Deleted";
	}

	@Override
	public String addFlavorToOption(long flavorId, long serviceId) throws NotFoundException, DuplicateKeyException {
		Optional<Flavor> flavor = flavorRepo.findById(flavorId);
		if(ObjectUtils.isEmpty(flavor))
			throw new NotFoundException("The Flavor \""+flavorId+"\""+" is not found");
		Optional<Services> service = serviceRepo.findById(serviceId);
		if(ObjectUtils.isEmpty(service))
			throw new NotFoundException("The Service \""+serviceId+"\""+" is not found");
		ServiceOption option = new ServiceOption();
		boolean dup = optionRepo.existsServiceOptionByFlavorAndService(flavor.get(),service.get());
		if(dup)
			throw new DuplicateKeyException("The option is duplicate with serviceId \""+serviceId+"\" and flavorId \""+flavorId+"\"");
		option.setFlavor(flavor.get());
		option.setService(service.get());		
		option = optionRepo.save(option);
		return ObjectUtils.isEmpty(option)?"ERROR" : "Passed";
	}
}
