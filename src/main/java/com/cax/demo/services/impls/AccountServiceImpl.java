package com.cax.demo.services.impls;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cax.demo.models.Account;
import com.cax.demo.models.Role;
import com.cax.demo.repositories.AccountRepository;
import com.cax.demo.repositories.RoleRepository;
import com.cax.demo.services.AccountService;
import com.cax.demo.utils.BcryptEncoder;



@Service
@Transactional

public class AccountServiceImpl implements AccountService, UserDetailsService {
	@Autowired
	private AccountRepository repo;
	@Autowired
	private RoleRepository roleRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = repo.findByUsername(username);
		if (account == null) {
			throw new UsernameNotFoundException("Account not found");
		} else {
		}
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
		account.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
		return new org.springframework.security.core.userdetails.User(account.getUsername(), account.getPassword(),
				authorities);
	}

	@Override
	public Account saveAccount(Account account) {
		account.setPassword(BcryptEncoder.passwordEncoder().encode(account.getPassword()));
		return repo.save(account);
	}

	@Override
	public Role saveRole(Role role) {
		return roleRepo.save(role);
	}

	@Override
	public void addRoleToUser(String username, String roleName) {
		Account account = repo.findByUsername(username);
		Role role = roleRepo.findByName(roleName);
	
		account.getRoles().add(role);
	}

	@Override
	public void updateRoleToUser(String username, String roleName) {
		Account account = repo.findByUsername(username);
		Role role = roleRepo.findByName(roleName);
		
		Collection<Role> roles = new ArrayList<Role>();
		roles.add(role);
		account.setRoles(roles);
		// account.getRoles().add(role);
	}

	@Override
	public Account getAccount(String username) {
		
		return repo.findByUsername(username);
	}

	@Override
	public Collection<Account> getAccounts() {
		
		return repo.findAll();
	}

	@Override
	public void saveToken(String username, String token) {
		Account account = repo.findByUsername(username);
		
		account.setRefreshToken(token);
		repo.save(account);
	}

	@Override
	public Account findByToken(String token) {
		return repo.findByRefreshToken(token);
	}

	
}
