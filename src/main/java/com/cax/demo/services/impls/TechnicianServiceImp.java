package com.cax.demo.services.impls;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.PhotoDTO;
import com.cax.demo.dtos.TechnicianDTO;
import com.cax.demo.models.Technician;
import com.cax.demo.repositories.TechnicianRepository;
import com.cax.demo.services.TechnicianService;


@Service
public class TechnicianServiceImp implements TechnicianService{

	@Autowired
	private TechnicianRepository repo;
	
	@Cacheable("technicians")
	@Override
	public Collection<?> getAll() {
		Collection<Technician> entities = repo.findAll();
		if(!ObjectUtils.isEmpty(entities)) {
			Collection<TechnicianDTO> result = new ArrayList<>();
			for (Technician technician : entities) {
			TechnicianDTO dto = new TechnicianDTO(technician.getId(),technician.getName(),technician.getAvatar());
			result.add(dto);
		}
			return result;
		}
		return null;
	}
	@Override
	@CacheEvict(value = "technicians", allEntries = true)
    public void cleanCache() {
    }
	
	@Override
	public void saveImage(MultipartFile imageFile, PhotoDTO photoDTO) throws IOException {
		
//		Path currentPath = Paths.get(".");
//		Path absolutePath = currentPath.toAbsolutePath();
//		System.out.println( "asolute "+absolutePath);
//		absolutePath = absolutePath.getParent();
//		System.out.println( "parent "+absolutePath);
//		absolutePath = absolutePath.getParent();
		//photoDTO.setPath(absolutePath + "/WEB-INF/classes/static/photos/");
		//photoDTO.setPath(absolutePath + "/webapps/deluxenails-be-0.0.1-SNAPSHOT/WEB-INF/classes/static/photos/");
		photoDTO.setPath("/opt/tomcat/webapps/deluxenails-be-0.0.1-SNAPSHOT/WEB-INF/classes/static/photos/");
		
		byte[] bytes = imageFile.getBytes();
		Path path = Paths.get(photoDTO.getPath() + imageFile.getOriginalFilename());
		Files.write(path, bytes);
	}
}
