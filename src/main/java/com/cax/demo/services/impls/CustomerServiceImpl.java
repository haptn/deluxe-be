package com.cax.demo.services.impls;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cax.demo.dtos.CustomerCheckinRequest;
import com.cax.demo.dtos.CustomerDTO;
import com.cax.demo.dtos.CustomerFilter;
import com.cax.demo.dtos.CustomerHistoryDTO;
import com.cax.demo.dtos.CutomerCheckOutRequest;
import com.cax.demo.dtos.Pagingnation;
import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;
import com.cax.demo.repositories.CustomCustomerRepo;
import com.cax.demo.repositories.CustomerHistoryRepository;
import com.cax.demo.repositories.CustomerRepository;
import com.cax.demo.services.CustomerService;
import com.cax.demo.utils.DateTimeFormater;
import com.cax.demo.utils.Mapper;

@Service
@EnableScheduling
public class CustomerServiceImpl implements CustomerService {
	private List<String> indexes = new ArrayList<>();

	@Autowired
	private CustomCustomerRepo repo;

	@Autowired
	private CustomerHistoryRepository historyRepo;

	@Autowired
	private CustomerRepository cusRepo;

	@Autowired
	EntityManager em;

	@Scheduled(cron = "0 0 0 * * *", zone = "America/Chicago")
	public void resetListQueue() {
		indexes.clear();
	}

	@Override
	public Pagingnation<CustomerDTO> getAll(CustomerFilter filter) {
		List<CustomerDTO> dtos = repo.getAll(filter);
		long size = repo.count(filter);
		dtos.forEach(dto -> dto
				.setIndex(indexes.lastIndexOf(dto.getPhone()) == -1 ? null : indexes.lastIndexOf(dto.getPhone())));
		Pagingnation<CustomerDTO> page = new Pagingnation<>(dtos, (int) Math.ceil(size / filter.getLimit()), size);
		return page;
	}

	@Override
	public List<CustomerHistoryDTO> getHistories(String phone) {
		List<CustomerHistoryDTO> dtos = repo.getAllHistoryByPhone(phone);
		return dtos;
	}

	@Override
	public Customer update(Customer customer) {
		Customer cus = cusRepo.save(customer);
		return cus;
	}

	@Override
	public CustomerDTO delete(String phone) {
		Optional<Customer> option = cusRepo.findByPhone(phone);
		Customer cus = option.get();
		if (Objects.isNull(cus))
			return null;
		cusRepo.delete(cus);
		CustomerDTO dto = Mapper.getMapper().map(cus, CustomerDTO.class);
		return dto;
	}

	@Override
	@Transactional
	public CustomerHistory updateHistory(CustomerHistory history) {
		CustomerHistory entity = em.find(CustomerHistory.class, history.getId());
		if (entity != null) {
			Long id = historyRepo.findCustomeridByHitoryId(entity.getId());
			Customer cus = cusRepo.getById(id);
			if (history.getName() != null && cus.getName() != null) {
				String name = cus.getName();
				name = name.replace(entity.getName(), history.getName());
				cus.setName(name);
				em.persist(cus);
			}
			entity = entity.update(history.getPrice(), history.getReward(), history.getDiscount(), history.getName());
			em.merge(entity);
			em.flush();
			return entity;
		}
		return null;
	}

	@Override
	public CustomerHistoryDTO deleteHistory(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Customer checkIn(CustomerCheckinRequest request, String user) throws ParseException {
		String sql = "SELECT * FROM customer as c WHERE EXISTS (SELECT * FROM customer_history as h"
				+ " WHERE c.id = h.customer_id and h.status = 'REQUEST' and c.phone = :phone and c.status= 'REQUEST')";
		String querySQL = "SELECT * FROM customer as c where c.phone= :phone";
		Query query = em.createNativeQuery(sql);
		query.setParameter("phone", request.getPhone());
		List<Object[]> customers = query.getResultList();
		CustomerHistory history = new CustomerHistory();
		String date = DateTimeFormater.getFormatter().format(new Date());
		history.setRequestTime(DateTimeFormater.getFormatter().parse(date));
		history.setStatus("REQUEST");
		if (user.equalsIgnoreCase("ADMIN")) {
			history.setStatus("CHECK-IN");
		}
		String name = request.getName();
		history.setName(name);
		Customer customer = null;
		BigInteger id = null;
		indexes.add(request.getPhone());
		history.setSpa(request.getSpa());
		if (customers.isEmpty()) {
			query = em.createNativeQuery(querySQL);
			query.setParameter("phone", request.getPhone());
			customers = query.getResultList();
			if (customers.isEmpty()) {
				customer = Customer.builder().address(request.getAddress()).birthDate(request.getBirthdate())
						.email(request.getEmail()).phone(request.getPhone()).type("NEW").status("REQUEST").name(name)
						.spa(request.getSpa()).build();
				customer.setStatus("REQUEST");
				if (user.equalsIgnoreCase("ADMIN")) {
					customer.setStatus("CHECK-IN");
					history.setCheckinTime(DateTimeFormater.getFormatter().parse(date));
				}
				history.setCustomer(customer);
				customer.addHistory(history);
				em.persist(customer);
				return customer;
			}
		}
		for (Object[] object : customers) {
			id = (BigInteger) object[0];
		}
		customer = em.find(Customer.class, id.longValue());
		customer.setSpa(request.getSpa());
		customer.addHistory(history);
		history.setCustomer(customer);
		if (name != null) {
			String[] names = customer.getName().split(", ");
			Stream<String> stream = Arrays.stream(names);
			if (!stream.anyMatch(e -> e.equalsIgnoreCase(name)))
				customer.setName(customer.getName() + ", " + name);
		}
		customer.setStatus("REQUEST");
		em.persist(history);
		return customer;
	}

	@Override
	public Customer details(long id) {
		Customer customer = em.find(Customer.class, id);
		return customer;
	}

	@Override
	public Integer getIndex(String phone) {
		return indexes.lastIndexOf(phone);
	}

	@Override
	public CustomerDTO checkIndexRequestUnConfirmed(String phone) {
		List<Customer> resultList = cusRepo.findRequestUnConfirmed(phone);
		if (!resultList.isEmpty()) {
			CustomerDTO dto = new CustomerDTO().convertEntityToDTO(resultList.get(0), indexes.lastIndexOf(phone));
			return dto;
		}
		return null;
	}

	@Override
	@Transactional
	public void confirm(Long id) throws ParseException, NullPointerException {
		Customer customer = em.find(Customer.class, id);
		CustomerHistory history = historyRepo.findByCustomerAndStatus(customer, "REQUEST");
		history.setStatus("CHECK-IN");
		String date = DateTimeFormater.getFormatter().format(new Date());
		history.setCheckinTime(DateTimeFormater.getFormatter().parse(date));
		customer.setStatus("CHECK-IN");
	}

	@Override
	@Transactional
	public CustomerDTO checkOut(CutomerCheckOutRequest request) throws ParseException, NullPointerException {
		Customer customer = em.find(Customer.class, request.getId());
		customer.setStatus("CHECK-OUT");
		Query query = em.createNativeQuery(
				"SELECT count(*) from customer_history as h where h.status='CHECK-OUT' and h.customer_id =:id");
		query.setParameter("id", request.getId());
		Integer result = Integer.parseInt(query.getSingleResult().toString());
		if (result >= 3 && customer.getType().equalsIgnoreCase("NEW")) {
			customer.setType("REGULAR");
		}
		if (result >= 19 && customer.getType().equalsIgnoreCase("REGULAR")) {
			customer.setType("VIP");
		}
		if (result >= 49 && customer.getType().equalsIgnoreCase("VIP")) {
			customer.setType("DIAMONDS VIP");
		}
		CustomerHistory history = historyRepo.findByCustomerAndStatus(customer, "CHECK-IN");
		history.setStatus("CHECK-OUT");
		history.setDiscount(request.getDiscount());
		history.setPrice(request.getPrice());
		history.setReward(request.getReward());
		String date = DateTimeFormater.getFormatter().format(new Date());
		history.setCheckoutTime(DateTimeFormater.getFormatter().parse(date));
		customer.getHistories().remove(customer.getHistories().stream().filter(x -> x.getId() == history.getId()));
		customer.addHistory(history);
		em.merge(customer);
		return new CustomerDTO().convertEntityToDTO(customer, history);
	}

	@Override
	public String[] getNameByPhone(String phone) {
		String sql = "select cus.name FROM customer as cus where cus.phone =:phone";
		Query query = em.createNativeQuery(sql);
		query.setParameter("phone", phone);
		String[] result = null;
		List<String> names = query.getResultList();
		if (!names.isEmpty()) {
			result = names.get(0).split(", ");
		}
		return result;
	}

	@Override
	public CustomerHistory detailsHistory(long id) {
		CustomerHistory history = em.find(CustomerHistory.class, id);
		return history;
	}

	@Override
	public List<String> getPhoneListByCheckinStatus(Integer limit) {
		List<Long> customerIds = historyRepo.findTop5ByStatusOrderByCheckoutTimeDesc("CHECK-OUT", limit);
		List<Customer> customers = cusRepo.findAllById(customerIds);
		List<String> phones = customers.stream().map(Customer::getPhone).collect(Collectors.toList());
		return phones;
	}

}
