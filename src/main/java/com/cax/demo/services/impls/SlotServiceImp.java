package com.cax.demo.services.impls;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cax.demo.dtos.Time;
import com.cax.demo.models.Slot;
import com.cax.demo.repositories.SlotRepository;
import com.cax.demo.services.SlotService;

@Service
public class SlotServiceImp implements SlotService {
	@Autowired
	private SlotRepository repo;
	
	@Override
	public Collection<?> getAll() {
	Collection<Slot> slots=  repo.findAll();
	Collection<Time> result = new ArrayList<Time>();
	for (Slot slot : slots) {
		result.add(new Time(slot.getId(), slot.getLabel(),slot.getValue()));
	}
	return result;
	}

}
