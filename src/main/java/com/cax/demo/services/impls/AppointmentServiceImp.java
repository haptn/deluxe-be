package com.cax.demo.services.impls;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.cax.demo.dtos.AppointmentDTO;
import com.cax.demo.dtos.AppointmentFilter;
import com.cax.demo.dtos.AppointmentStatus;
import com.cax.demo.dtos.PagingationRequest;
import com.cax.demo.dtos.Pagingnation;
import com.cax.demo.models.Appointment;
import com.cax.demo.models.Appointment_;
import com.cax.demo.models.Details;
import com.cax.demo.models.Details_;
import com.cax.demo.models.ServiceOption;
import com.cax.demo.models.ServiceOption_;
import com.cax.demo.models.Services;
import com.cax.demo.models.Services_;
import com.cax.demo.models.Technician;
import com.cax.demo.models.Technician_;
import com.cax.demo.repositories.AppointmentRepository;
import com.cax.demo.repositories.CustomSpecification;
import com.cax.demo.repositories.DetailRepository;
import com.cax.demo.repositories.ServiceOptionRepository;
import com.cax.demo.repositories.TechnicianRepository;
import com.cax.demo.services.AppointmentService;
import com.cax.demo.utils.DateTimeFormater;
import com.cax.demo.utils.Mapper;
import com.cax.demo.utils.SMTPMailSender;

@Service
public class AppointmentServiceImp implements AppointmentService {
	@Autowired
	private DetailRepository detailRepo;
	@Autowired
	private ServiceOptionRepository optionsRepo;
	@Autowired
	private AppointmentRepository repo;

	@Autowired
	private TechnicianRepository techRepo;

	@Override
	public boolean add(AppointmentDTO<Long> dto) throws ParseException {
		if (ObjectUtils.isEmpty(dto))
			return false;
		else {
			Collection<Long> options = dto.getOptions();

			if (ObjectUtils.isEmpty(dto.getOptions()) || options.size() < 1)
				return false;
			if (ObjectUtils.isEmpty(dto.getTechnicianId()))
				return false;
			Appointment entity = Mapper.getMapper().map(dto, Appointment.class);

			entity.setId(0);
			Technician tech = null;
			if (dto.getTechnicianId() != 0) {
				tech = techRepo.getById(dto.getTechnicianId());
			}
			entity.setTechnician(tech);
			entity.setStatus("pending");
			entity.setTime(DateTimeFormater.getFormatter().parse(dto.getTime()));
			Appointment result = repo.save(entity);

			if (ObjectUtils.isEmpty(result))
				return false;

			Collection<Details> details = new ArrayList<>();

			for (Object optionId : dto.getOptions()) {
				ServiceOption option = optionsRepo.getById((Long) optionId);
				Details detail = new Details();
				detail.setAppointment(result);
				detail.setServiceOption(option);
				details.add(detail);
			}
			Collection<Details> detailResult = detailRepo.saveAll(details);
			if (ObjectUtils.isEmpty(detailResult))
				return false;
			return true;
		}
	}

	@Override
	public boolean updateStatus(Long id, String status) {
		Appointment entity = repo.getById(id);
		if (status.equalsIgnoreCase("confirmed"))
			entity.setStatus("confirmed");
		else if (status.equalsIgnoreCase("canceled"))
			entity.setStatus("canceled");
		else
			return true;
		Appointment result = repo.save(entity);
		// Send email to mailbox
		Collection<String> serviceList = getServiceName(entity.getDetails());
		if (entity.getEmail() != null) {
			try {
				SMTPMailSender.sendMail(entity, serviceList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ObjectUtils.isEmpty(result);
	}

	@Override
	public Pagingnation<AppointmentDTO<String>> getAll(PagingationRequest page, String key, String status, String start,
			String end, String name, Long[] serviceIds, Long[] techIds) {

		Date startTime = null;
		Date endTime = null;
		if (!ObjectUtils.isEmpty(start) && !ObjectUtils.isEmpty(end))
			try {
				startTime = DateTimeFormater.getFormatter().parse(start);
				endTime = DateTimeFormater.getFormatter().parse(end);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		Page<Appointment> entities = getPaging(page, key, status, startTime, endTime, name, serviceIds, techIds);
		Collection<AppointmentDTO<String>> results = new ArrayList<>();
		if (entities.hasContent()) {
			for (Appointment appointment : entities.getContent()) {
				AppointmentDTO<String> dto = Mapper.getMapper().map(appointment, AppointmentDTO.class);
				String time = DateTimeFormater.getFormatter().format(appointment.getTime());
				dto.setTime(time);
				if (Objects.nonNull(appointment.getTechnician())) {
					dto.setAvatar(appointment.getTechnician().getAvatar());
					dto.setTechnicianName(appointment.getTechnician().getName());
				}
				dto.setOptions(getServiceName(appointment.getDetails()));
				results.add(dto);
			}
		}
		Pagingnation<AppointmentDTO<String>> result = new Pagingnation<>(results, entities.getTotalPages(),
				entities.getTotalElements());
		return result;
	}

	public Collection<String> getServiceName(Collection<Details> details) {
		if (ObjectUtils.isEmpty(details))
			return null;
		Collection<String> results = new ArrayList<>();
		for (Details detail : details) {
			ServiceOption option = detail.getServiceOption();
			Services service = option.getService();
			String groupName = service.getGroup().getName();
			String serviceName = service.getName();
			String result = null;
			if (ObjectUtils.isEmpty(option.getFlavor()))
				result = serviceName + " - " + groupName;
			else {
				String flavor = option.getFlavor().getName();
				result = serviceName + "(" + flavor + ")" + " - " + groupName;
			}
			results.add(result);
		}
		return results;
	}

	private Pageable paging(PagingationRequest page) {
		Sort sort = Sort.by(page.getSortBy().toString());
		if (Sort.Direction.DESC.equals(page.getDirection())) {
			sort = sort.descending();
		} else if (Sort.Direction.ASC.equals(page.getDirection())) {
			sort.ascending();
		}
		return PageRequest.of(page.getPage(), page.getLimit(), sort);
	}

	private Page<Appointment> getPaging(PagingationRequest page, String key, String status, Date start, Date end,
			String guestName, Long[] serviceIds, Long[] technicianIds) {
		return repo.findAll((root, query, builder) -> {
			Predicate p = builder.conjunction();
			Join<Appointment, Technician> app_tech = root.join(Appointment_.TECHNICIAN, JoinType.LEFT);
			if (!ObjectUtils.isEmpty(serviceIds) || !ObjectUtils.isEmpty(key)) {
				Join<Appointment, Details> app_detail = root.join(Appointment_.DETAILS);
				Join<Details, ServiceOption> detail_option = app_detail.join(Details_.SERVICE_OPTION);
				Join<ServiceOption, Services> service_option = detail_option.join(ServiceOption_.SERVICE);
				if (!ObjectUtils.isEmpty(key)) {
					Predicate name = builder.like(root.get(Appointment_.NAME), "%" + key + "%");
					Predicate phone = builder.like(root.get(Appointment_.PHONE), "%" + key + "%");
					Predicate email = builder.like(root.get(Appointment_.EMAIL), "%" + key + "%");
					Predicate service = builder.like(service_option.get(Services_.NAME), "%" + key + "%");
					p = builder.or(name, phone, email, service);
				}
				if (!ObjectUtils.isEmpty(serviceIds))
					p = builder.and(p, builder.or(service_option.get(Services_.ID).in(serviceIds)));
			}
			if (!ObjectUtils.isEmpty(technicianIds)) {
				boolean flat = false;
				for (Long long1 : technicianIds) {
					if (long1 == -1)
						flat = true;
				}
				Predicate normal = builder.or(app_tech.get(Services_.ID).in(technicianIds));
				if (flat) {
					Predicate anyOne = builder.isNull(app_tech.get(Technician_.ID));
					p = builder.and(p, builder.or(anyOne, normal));
				} else
					p = builder.and(p, normal);
			}
			if (!ObjectUtils.isEmpty(status)) {
				p = builder.and(p, builder.equal(root.get(Appointment_.STATUS), status));
			}
			if (!ObjectUtils.isEmpty(start) && !ObjectUtils.isEmpty(end)) {
				p = builder.and(p, builder.between(root.get(Appointment_.TIME), start, end));
			}
			if (!ObjectUtils.isEmpty(guestName)) {
				p = builder.and(p, builder.like(root.get(Appointment_.NAME), "%" + guestName + "%"));
			}
			query.distinct(true);
			return p;
		}, paging(page));

	}

	public Pagingnation<AppointmentDTO<String>> getExample(AppointmentFilter filter, PagingationRequest page) {
		Date startTime = null;
		Date endTime = null;
		try {
			if (!isEmptyString(filter.getStart()) && !isEmptyString(filter.getEnd())) {
				startTime = DateTimeFormater.getFormatter().parse(filter.getStart());
				endTime = DateTimeFormater.getFormatter().parse(filter.getEnd());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Specification<Appointment> conditions = Specification
				.where((Specification<Appointment>) CustomSpecification.likeKey(filter.getKey(), Appointment_.NAME))
				.or((Specification<Appointment>) CustomSpecification.likeKey(filter.getKey(), Appointment_.EMAIL))
				.or((Specification<Appointment>) CustomSpecification.likeKey(filter.getKey(), Appointment_.PHONE))
				.or(CustomSpecification.findAppointmentByServiceName(filter.getKey()))
				.and((Specification<Appointment>) CustomSpecification.equalKey(filter.getEnumStatus(),
						Appointment_.STATUS))
				.and((Specification<Appointment>) CustomSpecification.likeKey(filter.getName(), Appointment_.NAME))
				.and((Specification<Appointment>) CustomSpecification.betweenDate(startTime, endTime,
						Appointment_.TIME))
				.and(CustomSpecification.findAppoinmentByTechIds(filter.getTechnicianIds()))
				.and(CustomSpecification.findAppoinmentByServiceIds(filter.getServiceIds()));
		Page<Appointment> entities = repo.findAll(conditions, paging(page));
		Collection<AppointmentDTO<String>> results = new ArrayList<>();
		if (entities.hasContent()) {
			results = new ArrayList<>();
			for (Appointment appointment : entities.getContent()) {
				AppointmentDTO<String> dto = Mapper.getMapper().map(appointment, AppointmentDTO.class);
				String time = DateTimeFormater.getFormatter().format(appointment.getTime());
				dto.setTime(time);
				if (Objects.nonNull(appointment.getTechnician())) {
					dto.setAvatar(appointment.getTechnician().getAvatar());
					dto.setTechnicianName(appointment.getTechnician().getName());
				}
				dto.setOptions(getServiceName(appointment.getDetails()));
				results.add(dto);
			}
		}
		Pagingnation<AppointmentDTO<String>> result = new Pagingnation<>(results, entities.getTotalPages(),
				entities.getTotalElements());
		return result;
	}

	@Override
	public long getPendingAppointment(AppointmentStatus status) {
		return repo.countByStatus(status.toString());
	}

	private boolean isEmptyString(String string) {
		return string == null || string.isEmpty();
	}
}
