package com.cax.demo.services;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.CustomerImport;
import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;

@Service
public class CSVService {
	@Autowired
	private EntityManager em;

	@Transactional
	public List<CustomerImport> save(MultipartFile file) {
		try {
			List<CustomerImport> tutorials = CSVHelper.csvToTutorials(file.getInputStream());
			for (CustomerImport cus : tutorials) {
				Customer c = cus.convertToCustomer();
				if(cus.getVisitCount() > 49) {
					c.setType("DIAMONDS VIP");
				}
				else if(cus.getVisitCount() > 19) {
					c.setType("VIP");
				}
				else if(cus.getVisitCount() >3) {
					c.setType("REGULAR");
				}
				for (int i = 0; i < cus.getVisitCount(); i++) {
					Date date = cus.getLastVisited();
					Double discount = cus.getDisscount();
					Double reward = cus.getTotalPoint();
					if (i > 0) {
						date = new Date(date.getTime() - (1000 * i * 60 * 60 * 24));
						discount = 0.0;
						reward = 0.0;
					}
					
					CustomerHistory his = CustomerHistory.builder().name(c.getName()).checkinTime(date)
							.checkoutTime(date).requestTime(date).discount(discount).reward(reward).status("CHECK-OUT")
							.price(0.0).customer(c).build();
					c.addHistory(his);
				}
				em.persist(c);
			}
			return tutorials;
		} catch (IOException e) {
			throw new RuntimeException("fail to store csv data: " + e.getMessage());
		}
	}
}
