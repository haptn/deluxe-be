package com.cax.demo.dtos;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pagingnation<T> {
	private Collection<T> list;
	private int totalPage;
	private long totalElements;
}
