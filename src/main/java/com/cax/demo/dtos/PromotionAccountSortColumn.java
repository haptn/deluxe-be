package com.cax.demo.dtos;

import java.util.Locale;
import java.util.Optional;

import org.springframework.data.domain.Sort.Direction;

public enum PromotionAccountSortColumn {
	id,reward,lastModifedDate;

	/**
	 * Returns the {@link Comlumn} enum for the given {@link String} value.
	 *
	 * @param value
	 * @throws IllegalArgumentException in case the given value cannot be parsed into an enum value.
	 * @return
	 */
	public static  PromotionAccountSortColumn fromString(String value) {

		try {
			return  PromotionAccountSortColumn.valueOf(value.toLowerCase(Locale.US));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format(
					"Invalid value '%s' for orders given! Has to be either 'id, name, status, time' (case insensitive).", value), e);
		}
	}

	/**
	 * Returns the {@link Direction} enum for the given {@link String} or null if it cannot be parsed into an enum
	 * value.
	 *
	 * @param value
	 * @return
	 */
	public static Optional< PromotionAccountSortColumn> fromOptionalString(String value) {

		try {
			return Optional.of(fromString(value));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}
	
}
