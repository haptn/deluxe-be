package com.cax.demo.dtos;

import java.util.Collection;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PromotionDTO {
	private String phone;
	private Collection<String> user	; 
	private double reward;
	private Boolean isConfirmed;
	private String lastModifedDateStr;	
	private Date lastModifedDate;
	private double total;
}
