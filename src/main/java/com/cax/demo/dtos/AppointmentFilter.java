package com.cax.demo.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentFilter {
	private String key;
	private AppointmentStatus status;
	private String start;
	private String end;
	private String name;
	private Long[] serviceIds;
	private Long[] technicianIds;
	public String getEnumStatus() {
		try {
		return status.toString();
		}catch(Exception ex) {
			return null;
		}
	}
	
}
