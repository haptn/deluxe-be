package com.cax.demo.dtos;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TextMessage {
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


}
