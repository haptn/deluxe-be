package com.cax.demo.dtos;

import java.util.Date;

import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CustomerDTO {
	private Long id;
	private String fullName;
	private String phone;
	private String status;
	private String type;
	private Integer visitCount;
	private Date lastVisited;
	private Double totalPoint;
	private Double discount;
	private Integer index;
	private String address;
	private Date birthdate;
	private String email;
	private String spa;
	
	public CustomerDTO convertEntityToDTO(Customer customer, CustomerHistory history) {
		setId(customer.getId());
		setPhone(customer.getPhone());
		setFullName(customer.getName());
		setLastVisited(history.getCheckoutTime());
		setStatus(customer.getStatus());
//		setTotalPoint(history.getReward());
		setDiscount(history.getReward());
		setAddress(customer.getAddress());
		setBirthdate(customer.getBirthDate());
		setSpa(customer.getSpa());
		return this;
	}
	public CustomerDTO convertEntityToDTO(Customer customer, Integer index) {
		setId(customer.getId());
		setPhone(customer.getPhone());
		setFullName(customer.getName());	
		setStatus(customer.getStatus());
		setIndex(index);
		setSpa(customer.getSpa());
		return this;
	}
}
