package com.cax.demo.dtos;

import java.util.Collection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ServiceDTO extends BaseDTO {
	private String description;
	private Collection<?> list;
	private String image;
	private Double price;
	private boolean isFixedPrice;
	private String groupName;
	private String scope;
	private String note;
	private boolean isActive;
	public ServiceDTO(String name,String description, Collection<?> list) {
		super(name);
		this.list = list;
		this.description = description;
	}
	public ServiceDTO(Long id, String name) {
		super(id, name);
	}
	public ServiceDTO(long id, String name,Collection<?> list) {
		super(id, name);
		this.list = list;
	}
	public ServiceDTO(String name, Collection<?> list) {
		super(name);
		this.list = list;
	}
	public ServiceDTO(long id, String name, String description,Collection<?> list) {
		super(id,name);
		this.list = list;
		this.description = description;
	}
	public ServiceDTO(Long id, String name, Collection<?> list, String image, Double price,
			boolean isFixedPrice, String groupName, String scope, boolean isActive, String description, String note) {
		super(id, name);
		this.list = list;
		this.image = image;
		this.price = price;
		this.isFixedPrice = isFixedPrice;
		this.groupName = groupName;
		this.scope = scope;
		this.isActive = isActive;
		this.description = description;
		this.note = note;
	}

}

