package com.cax.demo.dtos;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.util.ObjectUtils;

import com.cax.demo.models.Customer;
import com.cax.demo.utils.DateTimeFormater;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerImport {
	private String id;
	private String fullName;
	private String phone;
	private String email;
	private Date lastVisited;
	private Integer birthDay;
	private Integer birthMonth;
	private Integer visitCount;
	private Double currentPoint;
	private Double totalPoint;
	private Double disscount;
	private Date birthDate;
	
	public CustomerImport convertBirthdate() {
		try {
			String date = LocalDate.of(1970, this.birthMonth, this.birthDay).toString();
			Date d = DateTimeFormater.getFormatterOnlyDate().parse(date);
			this.setBirthDate(d);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return this;
	}
	
	public CustomerImport convertDisscount() {
		setDisscount(totalPoint - currentPoint);
		return this;
	}
	
	public Customer convertToCustomer() {
		if(ObjectUtils.isEmpty(email)) {
			setEmail(null);
		}
		Customer cus = Customer.builder()
						.name(fullName)
						.phone(phone)
						.email(email)
						.birthDate(birthDate)
						.status("CHECK-OUT")
						.type("NEW")
						.build();
		return cus;
	}
}
