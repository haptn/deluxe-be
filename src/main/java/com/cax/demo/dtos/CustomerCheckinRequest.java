package com.cax.demo.dtos;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CustomerCheckinRequest {
	private String phone;
	private String name;
	private Date birthdate;
	private String email;
	private String address;
	private String spa = "deluxe";
}
