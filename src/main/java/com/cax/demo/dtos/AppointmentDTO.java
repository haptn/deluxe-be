package com.cax.demo.dtos;

import java.util.Collection;
import java.util.Date;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Service
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDTO<T> extends BaseDTO{
	private String time;
	private String phone;
	private String email;
	private String note;
	private String status;
	private Collection<T> options;
	private Long technicianId;
	private String technicianName;
	private String avatar;
	public AppointmentDTO(Long id, String name, String time, String phone, String email, String note,
			Collection<T> options, Long technicianId,String createdBy,Date createdDate,String lastModifedBy,Date lastModifedDate) {
		super(id, name,createdBy,createdDate,lastModifedBy,lastModifedDate);
		this.time = time;
		this.phone = phone;
		this.email = email;
		this.note = note;
		this.options =options;
		this.technicianId = technicianId;
	}
	
}
