package com.cax.demo.dtos;

import java.util.Locale;
import java.util.Optional;

public enum AppointmentStatus {
	pending,confirmed,canceled;
	public static AppointmentStatus fromString(String value) {

		try {
			return AppointmentStatus.valueOf(value.toLowerCase(Locale.US));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format(
					"Invalid value '%s' for orders given! Has to be either 'id, name, status, time' (case insensitive).", value), e);
		}
	}
	public static Optional<AppointmentStatus> fromOptionalString(String value) {

		try {
			return Optional.of(fromString(value));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}
}
