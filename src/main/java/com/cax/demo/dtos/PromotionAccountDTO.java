package com.cax.demo.dtos;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PromotionAccountDTO extends BaseDTO{
	private String phone;
	private double reward;
	private Collection<String> accounts;
	private double changedPoint;
}
