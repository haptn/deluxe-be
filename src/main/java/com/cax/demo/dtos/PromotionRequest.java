package com.cax.demo.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
@Setter
@AllArgsConstructor
public class PromotionRequest {
	private String phone;
	private Double changedPoint;
	private String username;
	private String description;
}
