package com.cax.demo.dtos;

import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DayDTO {
	private String name;
	private Collection<Long> slotIds;
}
