package com.cax.demo.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CutomerCheckOutRequest {
	private Long id;
	private Double reward;
	private Double discount;
	private Double price;
}
