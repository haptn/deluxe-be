package com.cax.demo.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class TechnicianDTO extends BaseDTO{
	private String avatar;

	public TechnicianDTO(Long id, String name, String avatar) {
		super(id, name);
		this.avatar = avatar;
	}
}
