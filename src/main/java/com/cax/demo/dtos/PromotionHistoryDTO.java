package com.cax.demo.dtos;

import java.util.Date;

public interface PromotionHistoryDTO {
	public double getPoint();
	public String getFullName();
	public boolean isConfirmed();
	public Date getLastModifedDate();
	public Date getCreatedDate();
	public long getId();
}
