package com.cax.demo.dtos;

import org.springframework.data.domain.Sort;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationPromotionRequest {
	private int page = 0;
	private int limit =10;
	private Sort.Direction direction = Sort.Direction.DESC;
	private PromotionAccountSortColumn sortBy = PromotionAccountSortColumn.lastModifedDate;
}
