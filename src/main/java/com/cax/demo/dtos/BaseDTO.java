package com.cax.demo.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseDTO {
	private Long id;
	private String name;
	private String createdBy;
	private Date createdDate;
	private String lastModifedBy;
	private Date lastModifedDate;
	public BaseDTO(String name) {
		this.name = name;
	}
	public BaseDTO(Long id,String name) {
		this.name = name;
		this.id = id;
	}
}
