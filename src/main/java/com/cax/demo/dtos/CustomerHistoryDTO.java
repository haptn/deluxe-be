package com.cax.demo.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerHistoryDTO {
	private long id;
	private Date checkinTime;
	private Date checkoutTime;
	private Double discount;
	private Double price;
	private Date requestTime;
	private Double reward;
	private String status;
	private String spa;
}
