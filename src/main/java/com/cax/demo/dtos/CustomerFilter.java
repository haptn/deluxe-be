package com.cax.demo.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerFilter {
	private int page = 0;
	private int limit =15;
	private String direction = "desc";
	private String sortBy = "last_visited";
	private String name;
	private String phone;
	private String type;
	private String status;
	private String spa;
	private Long id;
}
