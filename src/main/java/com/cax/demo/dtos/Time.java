package com.cax.demo.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Time extends BaseDTO{
	private int value;

	public Time(Long id, String name, int value) {
		super(id, name);
		this.value = value;
	}

	
}
