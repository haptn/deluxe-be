package com.cax.demo.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PromotionAccountFilter {
	private String phone;
	private String fullName;
}
