package com.cax.demo.utils;

import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Locale;

public class DateTimeFormater {
	private static SimpleDateFormat sdf;
	private static SimpleDateFormat ssdf;
	private static SimpleDateFormat ssdfff;
	private static SimpleDateFormat dte;
	private static SimpleDateFormat hh;
	
	public static SimpleDateFormat getFormatter() {
		if(sdf==null) {
			synchronized (DateTimeFormater.class) {
				if(sdf==null) {
					sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sdf.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return sdf;
	}
	public static SimpleDateFormat getFormatterOnlyDate() {
		if(ssdf==null) {
			synchronized (DateTimeFormater.class) {
				if(ssdf==null) {
					ssdf = new SimpleDateFormat("yyyy-MM-dd");
				ssdf.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return ssdf;
	}
	public static SimpleDateFormat getFormatterCSV() {
		if(ssdf==null) {
			synchronized (DateTimeFormater.class) {
				if(ssdf==null) {
					ssdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				ssdf.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return ssdf;
	}
	public static SimpleDateFormat getFormatterCSV2() {
		if(ssdfff == null) {
			synchronized (DateTimeFormater.class) {
				if(ssdfff == null) {
					ssdfff = new SimpleDateFormat("M/d/yy HH:mm");
				ssdfff.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return ssdf;
	}
	public static SimpleDateFormat getFormatterEEEE() {
		if(dte == null) {
			synchronized (DateTimeFormater.class) {
				if(dte == null) {
					dte = new SimpleDateFormat("EEEE, MMMM d", Locale.US);
					dte.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return dte;
	}
	
	public static SimpleDateFormat getFormatterForHH() {
		if(hh == null) {
			synchronized (DateTimeFormater.class) {
				if(hh == null) {
					hh = new SimpleDateFormat("hh:mm a", Locale.US);
					hh.setTimeZone(TimeZone.getTimeZone("America/Chicago"));
				}
			}
		}
		return hh;
	}
}
