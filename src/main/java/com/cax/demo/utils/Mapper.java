package com.cax.demo.utils;

import org.modelmapper.ModelMapper;

public class Mapper {
	private static ModelMapper mapper;
	public static ModelMapper getMapper() {
		if(mapper==null) {
			synchronized (Mapper.class) {
				if(mapper == null)
					mapper = new ModelMapper();
			}
		}
		return mapper;
	}
}
