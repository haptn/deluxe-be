package com.cax.demo.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptEncoder {
	private static BCryptPasswordEncoder bCrypt;
	public static BCryptPasswordEncoder passwordEncoder() {
		if(bCrypt==null) {
			synchronized (BcryptEncoder.class) {
				if(bCrypt==null) {
					bCrypt = new BCryptPasswordEncoder();
				}
			}
		}
		return bCrypt;
	}
}
