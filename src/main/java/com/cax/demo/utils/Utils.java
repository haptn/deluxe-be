package com.cax.demo.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.cax.demo.dtos.PaginationPromotionRequest;

public class Utils {
	public static Pageable paging(PaginationPromotionRequest page) {
		Sort sort = Sort.by(page.getSortBy().toString());
		if (Sort.Direction.DESC.equals(page.getDirection())) {
			sort = sort.descending();
		} else if (Sort.Direction.ASC.equals(page.getDirection())) {
			sort.ascending();
		}
		return PageRequest.of(page.getPage(), page.getLimit(), sort);
	}
}
