package com.cax.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.cax.demo.filters.CustomAthorizationFilter;
import com.cax.demo.filters.CustomAuthenticationFilter;
import com.cax.demo.services.AccountService;
import com.cax.demo.utils.BcryptEncoder;

import lombok.RequiredArgsConstructor;


@Configuration
@EnableWebSecurity @RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailSevice;
	@Autowired
	private AccountService accountSevice;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailSevice).passwordEncoder(BcryptEncoder.passwordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		CustomAuthenticationFilter customFilter = new CustomAuthenticationFilter(authenticationManagerBean());
//		customFilter.setFilterProcessesUrl(null);
		http.cors().and().csrf().disable();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
		//.antMatchers("/**").permitAll()
		.antMatchers("/admin/**").hasAuthority("ROLE_ADMIN");		
		http.addFilter(new CustomAuthenticationFilter(authenticationManagerBean(),accountSevice));
		http.addFilterBefore(new CustomAthorizationFilter(),UsernamePasswordAuthenticationFilter.class);
	}
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManagerBean();
	}
	
//	  	@Bean
//	    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//	        // Customize the application security ...
//	        http.requiresChannel().anyRequest().requiresSecure();
//	        return http.build();
//	    }

	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                		.allowCredentials(false)
                		.allowedOrigins("*")
                		.allowedHeaders("*")
                        .allowedMethods("GET", "POST", "HEAD", "PUT", "DELETE", "OPTIONS", "PATCH");
            }
        };
    }
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
		.antMatchers("/v2/api-docs",
		"/configuration/ui",
		"/swagger-resources/**",
		"/configuration/security",
		"/swagger-ui.html",
		"/webjars/**");
	}	 
}
