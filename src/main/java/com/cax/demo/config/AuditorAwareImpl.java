package com.cax.demo.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		 if (authentication == null || !authentication.isAuthenticated()) {
			   return null;
			  }
		 String userDetails = null;
		 if(!authentication.getPrincipal().equals("anonymousUser")){
			userDetails =   (String) authentication.getPrincipal();
		 }else {
			 userDetails ="anonymousUser";
		 }		
			  return Optional.ofNullable(userDetails);
	}
}
	