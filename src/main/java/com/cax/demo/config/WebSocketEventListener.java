package com.cax.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

@Component
public class WebSocketEventListener {
 
	
	
	@Autowired
	private SimpMessageSendingOperations messagingTemplate;
	
	@EventListener
	 public void handleWebSocketConnectListener(SessionConnectedEvent event) {
       
    }
	
//	  @EventListener
//	    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
//	        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
//
//	        String username = (String) headerAccessor.getSessionAttributes().get("username");
//	        if(username != null) {
//	            logger.info("User Disconnected : " + username);
//
//	            ChatMessage chatMessage = new ChatMessage();
//	            chatMessage.setType(ChatMessage.MessageType.LEAVE);
//	            chatMessage.setSender(username);
//
//	            messagingTemplate.convertAndSend("/topic/public", chatMessage);
//	        }
//	    }
}
