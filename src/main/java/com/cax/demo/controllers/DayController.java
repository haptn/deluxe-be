package com.cax.demo.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.services.DayService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@RestController
@RequestMapping("time")
public class DayController {
	
	@Autowired
	private DayService service;
	
	@GetMapping
	@ApiParam(value = "Date-(String) có thể là thứ (1,2,3,..7) hoặc ngày cụ thể(2012-07-21) hoặc default(0)", required = true) 
    @ApiOperation("Get time slot by date")
	public Collection<?> getTimeByDate(@RequestParam String date){
		return service.getSlot(date);
	}
}
