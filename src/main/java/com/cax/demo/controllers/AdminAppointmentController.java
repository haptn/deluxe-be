package com.cax.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.dtos.AppointmentFilter;
import com.cax.demo.dtos.AppointmentStatus;
import com.cax.demo.dtos.PagingationRequest;
import com.cax.demo.services.AppointmentService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin("*")
@RestController
@RequestMapping("admin/appointments")
public class AdminAppointmentController {
	@Autowired
	private AppointmentService service;

	@PatchMapping("/{id}")
	@ApiParam(value = "status includes (confirmed/cancelled)", required = true)
	@ApiOperation("Update status to (confirmed/cancelled)")
	public ResponseEntity<String> updateStatus(@PathVariable Long id, @RequestParam String status) {
		boolean result = false;
		try {
			result = service.updateStatus(id, status);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		return result ? new ResponseEntity<String>("failed", HttpStatus.BAD_REQUEST)
				: new ResponseEntity<String>("completed", HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAll(PagingationRequest page, AppointmentFilter filter) {
		
			return ResponseEntity.ok(service.getExample(filter, page));
		
	}

	@GetMapping("/status")
	public long getPendingAppointment(AppointmentStatus status) {
		return service.getPendingAppointment(status);
	}
}
