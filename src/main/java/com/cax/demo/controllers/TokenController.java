package com.cax.demo.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.cax.demo.models.Account;
import com.cax.demo.models.Role;
import com.cax.demo.services.AccountService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin
@RestController
@RequestMapping("token/refresh")
public class TokenController {
	
	@Autowired
	private AccountService service;
	
	@GetMapping
	public void refeshToken(HttpServletRequest request, HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException{
		String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		Map<String,String> error = new HashMap<>();
			if(authorizationHeader !=null && authorizationHeader.startsWith("Bearer ")) {
				String refresh_token= null;
				Algorithm algorithm = Algorithm.HMAC256("HA".getBytes());
				JWTVerifier verifier= JWT.require(algorithm).build();
				DecodedJWT decodedJWT = null;
				String accessToken = null;
				Account user = null;
				Map<String,String> tokens = new HashMap<>();
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				try {
					refresh_token= authorizationHeader.substring("Bearer ".length());	
					decodedJWT = verifier.verify(refresh_token);
					String username =decodedJWT.getSubject();
					user = service.getAccount(username);
					accessToken = generateAccessToken(user, algorithm, request);
		tokens.put("accessToken",accessToken);
		tokens.put("refreshToken",refresh_token);
		
		new ObjectMapper().writeValue(response.getOutputStream(),tokens);
				}catch(Exception e) {
					if(e.getMessage().contains("The Token has expired")) {
						user = service.findByToken(refresh_token);
						if(user!= null) {
							accessToken = generateAccessToken(user, algorithm, request);
							refresh_token = generateRefreshToken(user, algorithm, request);
							tokens.put("accessToken",accessToken);
							tokens.put("refeshToken",refresh_token);
							service.saveToken(user.getUsername(), refresh_token);
							new ObjectMapper().writeValue(response.getOutputStream(),tokens);
						}else {
							response.setStatus(HttpStatus.FORBIDDEN.value());
							error.put("error_message","Refresh token is invalid");
							new ObjectMapper().writeValue(response.getOutputStream(),error);
						}
					}	
				}		
			}else {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			error.put("error_message","Refresh token is missing");
			new ObjectMapper().writeValue(response.getOutputStream(),error);
			}
		}
	
	private String generateAccessToken(Account user, Algorithm algorithm, HttpServletRequest request) {
		String access_token = JWT.create()
				.withSubject(user.getUsername())
				.withExpiresAt(new Date(System.currentTimeMillis() + 15 * 60 * 1000))
				.withIssuer(request.getRequestURI().toString())
				.withClaim("roles", user.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
				.sign(algorithm);
		return access_token;
	}
	private String generateRefreshToken(Account user, Algorithm algorithm, HttpServletRequest request) {	
		String refresh_token = JWT.create()
				.withSubject(user.getUsername())
				.withExpiresAt(new Date(System.currentTimeMillis() + 120 * 60 * 1000))
				.withIssuer(request.getRequestURI().toString())
				.sign(algorithm);	
		return refresh_token;
	}	

}
