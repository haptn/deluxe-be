package com.cax.demo.controllers;

import java.net.URI;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cax.demo.models.Account;
import com.cax.demo.models.Role;
import com.cax.demo.services.AccountService;

import lombok.Data;

@CrossOrigin
@RestController
@RequestMapping("admin/accounts")
public class AdminAccountController {
	@Autowired
	private AccountService service;

	@GetMapping
	public ResponseEntity<?> getAccount() {
		try {
			return ResponseEntity.ok().body(service.getAccounts());
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@PostMapping
	public ResponseEntity<?> saveAccount(@RequestBody Account account) {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/accounts").toUriString());
		return ResponseEntity.created(uri).body(service.saveAccount(account));
	}

	@PostMapping("roles")
	public ResponseEntity<?> saveRole(@RequestBody Role role) {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/roles").toUriString());
		return ResponseEntity.created(uri).body(service.saveRole(role));
	}

	@PostMapping("roles/addtouser")
	public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form) {
		service.addRoleToUser(form.getUsername(), form.getRoleName());
		return ResponseEntity.ok().build();
	}

	@PutMapping("roles/updatetouser")
	public ResponseEntity<?> updateRoleToUser(@RequestBody RoleToUserForm form) {
		service.updateRoleToUser(form.getUsername(), form.getRoleName());
		return ResponseEntity.ok().build();
	}

}

@Data
class RoleToUserForm {
	private String username;
	private String roleName;
}
