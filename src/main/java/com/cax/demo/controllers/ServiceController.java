package com.cax.demo.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.services.ServiceOptionService;


@CrossOrigin
@RestController
@RequestMapping("services")
public class ServiceController {
	@Autowired
	private ServiceOptionService service;
	@GetMapping
	public Collection<?> getAllServices(){
		return service.getAllServices();
	}
	@GetMapping("/clean-cache")
	public String cleanCache(){
		service.cleanCache();
		return "Completed !!!";
	}
	@GetMapping("/menu")
	public Collection<?> getAllMenuServices(){
		return service.getMenuServices();
	}

}
