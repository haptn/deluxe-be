package com.cax.demo.controllers;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.CustomerCheckinRequest;
import com.cax.demo.dtos.CustomerDTO;
import com.cax.demo.dtos.CustomerFilter;
import com.cax.demo.dtos.CustomerImport;
import com.cax.demo.dtos.CutomerCheckOutRequest;
import com.cax.demo.dtos.TextMessage;
import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;
import com.cax.demo.services.CSVHelper;
import com.cax.demo.services.CSVService;
import com.cax.demo.services.CustomerService;
import com.cax.demo.utils.PersistenceUtils;

@RestController
@RequestMapping("checkIn-Out")
@CrossOrigin
@EnableAsync
public class CustomerController {
	@Autowired
	private CustomerService service;

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Async
	private void sendMessage() throws InterruptedException {
		Thread.sleep(500);
		simpMessagingTemplate.convertAndSend("/topic/message", new TextMessage("message"));

	}

	@PostMapping("/request")
	public ResponseEntity<?> requestCheckIn(@RequestBody CustomerCheckinRequest request) {
		CustomerDTO requestIndex = service.checkIndexRequestUnConfirmed(request.getPhone());
		try {
			if (requestIndex == null) {
				Customer customer = service.checkIn(request, "guest");
				Integer index = service.getIndex(request.getPhone());
				CustomerDTO dto = new CustomerDTO().convertEntityToDTO(customer, index);
				sendMessage();
				return ResponseEntity.ok().body(dto);
			}
			return ResponseEntity.badRequest().body(requestIndex);
		} catch (ParseException | InterruptedException e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@GetMapping
	public ResponseEntity<?> getAll(CustomerFilter filter) {
		return ResponseEntity.ok(service.getAll(filter));
	}

	@PatchMapping("/{id}/confirm")
	public ResponseEntity<?> confirm(@PathVariable Long id) {
		try {
			service.confirm(id);
			return ResponseEntity.ok().build();
		} catch (NullPointerException ex) {
			return ResponseEntity.notFound().build();
		} catch (ParseException e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@PatchMapping("/{id}/checkout")
	public ResponseEntity<?> checkout(@PathVariable Long id, @RequestBody CutomerCheckOutRequest request) {
		request.setId(id);
		try {
			CustomerDTO response = service.checkOut(request);
			return ResponseEntity.ok().body(response);
		} catch (NullPointerException ex) {
			return ResponseEntity.notFound().build();
		} catch (ParseException e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@GetMapping("/{phone}/get-history")
	public ResponseEntity<?> getHistory(@PathVariable String phone) {
		return ResponseEntity.ok(service.getHistories(phone));
	}

	@PatchMapping("/{id}/update")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Customer request) {
		try {
			Customer currentUser = service.details(id);
			if (currentUser == null) {
				return ResponseEntity.notFound().build();
			}
			request = (Customer) PersistenceUtils.partialUpdate(currentUser, request);
			request.setId(id);
			return ResponseEntity.ok(service.update(request));
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@GetMapping("/{phone}/names")
	public ResponseEntity<?> getNames(@PathVariable String phone) {
		try {
			String[] currentUser = service.getNameByPhone(phone);
			if (currentUser == null) {
				return ResponseEntity.notFound().build();
			}
			return ResponseEntity.ok(currentUser);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@PatchMapping("/{id}/update-history")
	public ResponseEntity<?> updateHistory(@PathVariable Long id, @RequestBody CustomerHistory request) {
		try {
			CustomerHistory currentUser = service.detailsHistory(id);
			if (currentUser == null) {
				return ResponseEntity.notFound().build();
			}
			request = (CustomerHistory) PersistenceUtils.partialUpdate(currentUser, request);
			request.setId(id);
			return ResponseEntity.ok(service.updateHistory(request));
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@Autowired
	private CSVService fileService;

	@PostMapping("/upload")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
		String message = "";
		if (CSVHelper.hasCSVFormat(file)) {
			List<CustomerImport> importData = fileService.save(file);
			// message = "Uploaded the file successfully: " + file.getOriginalFilename();
			return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.OK).body(importData);
		}
		message = "Please upload a csv file!";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
	}

	@PostMapping("/admin/request-confirm")
	public ResponseEntity<?> confirm(@RequestBody CustomerCheckinRequest request) {
		CustomerDTO requestIndex = service.checkIndexRequestUnConfirmed(request.getPhone());
		try {
			if (requestIndex == null) {
				Customer customer = service.checkIn(request, "admin");
				Integer index = service.getIndex(request.getPhone());
				CustomerDTO dto = new CustomerDTO().convertEntityToDTO(customer, index);
				sendMessage();
				return ResponseEntity.ok().body(dto);
			}
			return ResponseEntity.badRequest().body(requestIndex);
		} catch (ParseException | InterruptedException e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@DeleteMapping("/{phone}")
	public ResponseEntity<?> deleteCustomer(@PathVariable String phone) {
		try {
			CustomerDTO dto = service.delete(phone);
			if (dto == null) {
				return ResponseEntity.notFound().build();
			}
			return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.OK).body(dto);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@GetMapping("/phone")
	public ResponseEntity<?> getPhones(@RequestParam("limit") Integer limit) {
		try {
			List<String> list = service.getPhoneListByCheckinStatus(limit);
			return (ResponseEntity<?>) ResponseEntity.status(HttpStatus.OK).body(list);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

}
