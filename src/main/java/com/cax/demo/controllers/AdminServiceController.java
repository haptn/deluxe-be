package com.cax.demo.controllers;

import java.net.URI;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cax.demo.exceptions.DuplicateKeyException;
import com.cax.demo.exceptions.NotFoundException;
import com.cax.demo.services.ServiceOptionService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin
@RestController
@RequestMapping("admin/services")
public class AdminServiceController {
	@Autowired
	private ServiceOptionService service;
	@GetMapping
	public Collection<?> getAllServices(){
		return service.getAllServiceForAdmin();
	}
	@PostMapping("flavor")
	@ApiOperation("Thêm mới flavor và nối service với flavor đó")
	public ResponseEntity<?> addNewFlavor(String flavorName, long serviceId) throws NotFoundException, DuplicateKeyException {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/service").toUriString());
		return ResponseEntity.created(uri).body(service.addNewFlavor(flavorName, serviceId));
	}
	@PostMapping
	@ApiOperation("Nối service với flavor có sẵn")
	public ResponseEntity<?> addFlavorToOption(long serviceId, long flavorId) throws NotFoundException, DuplicateKeyException {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/service").toUriString());
		return ResponseEntity.created(uri).body(service.addFlavorToOption(flavorId, serviceId));
	}
	@DeleteMapping("/{optionId}")
    @ApiOperation("Remove an option of service by the option ID")
	public ResponseEntity<?> deleteOptionFlavor(@PathVariable long optionId) throws NotFoundException, DuplicateKeyException {
		return ResponseEntity.ok(service.removeOpionFlavor(optionId));
	}
	@DeleteMapping("flavor/{optionId}")
    @ApiOperation("Xóa flavor kể cả khi nó đang nằm trong service khác")
	public ResponseEntity<?> deleteAllFlavor(@PathVariable long optionId) throws NotFoundException, DuplicateKeyException {
		return ResponseEntity.ok(service.removeFlavor(optionId));
	}
}
