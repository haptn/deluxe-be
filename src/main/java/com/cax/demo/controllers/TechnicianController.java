package com.cax.demo.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cax.demo.dtos.PhotoDTO;
import com.cax.demo.services.TechnicianService;

@CrossOrigin
@RestController
@RequestMapping("technicians")
public class TechnicianController {
	@Autowired
	private TechnicianService service;
	
	@GetMapping
	public Collection<?> getAll(HttpServletRequest request){
		return service.getAll();
	}
	@GetMapping("/clean-cache")
	public String cleanCache(){
		service.cleanCache();
		return "Completed !!!";
	}
	
	@PostMapping("/uploadImage")
	public String uploadImage(@RequestParam("imageFile") MultipartFile imageFile) {
		String returnValue = "start";	
		PhotoDTO photoDTO = new PhotoDTO();
		photoDTO.setFileName(imageFile.getOriginalFilename());		
		try {
			service.saveImage(imageFile, photoDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnValue = e.getLocalizedMessage();
		}
		return returnValue;
	}
}
