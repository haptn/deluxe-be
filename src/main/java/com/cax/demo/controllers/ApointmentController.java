package com.cax.demo.controllers;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.dtos.AppointmentDTO;
import com.cax.demo.services.AppointmentService;

@CrossOrigin @RestController @RequestMapping("appointments")
public class ApointmentController {
	@Autowired
	private AppointmentService service;
	@PostMapping
	public String booking(@RequestBody AppointmentDTO<Long> dto) {
		boolean result;
		try {
			result = service.add(dto);
		} catch (ParseException e) {
			return "Failed";
		}
		return result ? "Completed" : "Failed";
	}
}
