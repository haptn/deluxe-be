package com.cax.demo.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.services.SlotService;

@CrossOrigin
@RestController
@RequestMapping("admin/slot")
public class AdminSlotController {
	
	@Autowired
	private SlotService service;
	
	@GetMapping
	private Collection<?> getAll(){
		return service.getAll();
	}
}
