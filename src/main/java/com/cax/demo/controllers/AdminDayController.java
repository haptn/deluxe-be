package com.cax.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.cax.demo.dtos.DayDTO;
import com.cax.demo.services.DayService;

@CrossOrigin
@RestController
@RequestMapping("admin/time")
public class AdminDayController {
	@Autowired
	private DayService service;
	
	@PostMapping()
	public ResponseEntity<?> insertTime(@RequestBody DayDTO dto) {
		try {
		service.save(dto);
		return new ResponseEntity<String>("Thành công", HttpStatus.OK);
		}catch(Exception e) {
		return new ResponseEntity<String>("Lỗi Back-end", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PatchMapping("/{name}")
	public ResponseEntity<?> recoverTime(@PathVariable String name) {
		try {
		service.recover(name);
		return new ResponseEntity<String>("Thành công", HttpStatus.OK);
		}catch(Exception e) {
		return new ResponseEntity<String>("Lỗi Back-end", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
