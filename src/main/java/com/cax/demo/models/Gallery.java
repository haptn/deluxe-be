package com.cax.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Entity
@Getter
@Setter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Gallery extends Base {
	@Column(length = 150)
	private String title;
	
	@OneToMany(mappedBy  ="gallery", fetch = FetchType.LAZY)
	private Collection<Photo> photos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="collection_id")
	private GalleryCollection galleryCollection;

}
