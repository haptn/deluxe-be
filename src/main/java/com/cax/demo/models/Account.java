package com.cax.demo.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
public class Account {
	@Id
	@Column(length = 15)
	private String username;
	@Column(nullable = false)
	@JsonIgnore
	private String password;
	@Column(length = 50)
	private String fullName;
	private String refreshToken;
	@ManyToMany
	@JsonIgnore
	@Fetch(org.hibernate.annotations.FetchMode.SELECT)
	private Collection<Role> roles = new ArrayList<>();
	
}
