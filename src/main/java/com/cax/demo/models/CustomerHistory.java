package com.cax.demo.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
public class CustomerHistory extends Base {
	@JsonIgnore
	private Date checkinTime;
	private Double price;
	private Double discount;
	@JsonIgnore
	private Date requestTime;
	@JsonIgnore
	private Date checkoutTime;
	private Double reward;
	@JsonIgnore
	private String status;
	private String name;
	private String spa;
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	public CustomerHistory update(Double price, Double discount, Double reward, String name) {
		if (price != null)
			this.price = price;
		if (reward != null)
			this.reward = reward;
		if (discount != null)
			this.discount = discount;
		if (name != null)
			this.name = name;
		return this;
	}
}
