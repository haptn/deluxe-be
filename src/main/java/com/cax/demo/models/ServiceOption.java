package com.cax.demo.models;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ServiceOption extends Base {
	@Column(length = 7)
	private String status;
	
	@OneToMany(mappedBy  ="serviceOption", fetch = FetchType.LAZY)
	private Collection<Details> details;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="flavor_id")
	private Flavor flavor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="service_id")
	private Services service;
	
}
