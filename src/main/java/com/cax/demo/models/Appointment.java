package com.cax.demo.models;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Cacheable
@EntityListeners(AuditingEntityListener.class)
public class Appointment extends Base {
	private Date time;

	@Column(length = 7)
	private String status;

	@Column(length = 25)
	private String phone;

	@Column(length = 100)
	private String email;

	private String note;
	
	@OneToMany(mappedBy = "appointment", fetch = FetchType.LAZY)
	private Collection<Details> details;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "technician_id")
	
	private Technician technician;
	@Column(length = 100)
	
	private String name;
}
