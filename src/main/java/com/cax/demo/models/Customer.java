package com.cax.demo.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
//import javax.persistence.PreRemove;
//import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
//@SQLDelete(sql = "update customer set is_deleted=true where id=?")
public class Customer extends Base{
	private String phone;
	private String name;
	private String email;
	private String address;
	@JsonIgnore
	private boolean isDeleted;
	private Date birthDate;
	private String type;
	private String status;
	private String spa;
//	@PreRemove
//	public void preRemove() {
//		this.isDeleted = true;
//	}
	public void addHistory(CustomerHistory history) {
		histories.add(history);
	}
	@JsonIgnore
	@OneToMany(mappedBy="customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Builder.Default
	private List<CustomerHistory> histories = new ArrayList<CustomerHistory>();
}
