package com.cax.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Services extends Base {
	private String image;
	private String description;
	private String note;
	@Column(nullable = true)
	private Double price;
	private boolean isFixedPrice;
	private String scope;
	private boolean isActive;
	@OneToMany(mappedBy="service", fetch = FetchType.LAZY)
	private Collection<ServiceOption> options;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="group_id")
	private ServiceGroup group;

	@Column(length = 100)
	private String name;

}
