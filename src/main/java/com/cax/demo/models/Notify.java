package com.cax.demo.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Notify  {
	@Id
	private String id;
	private String message;
	private String dateTime;
}
