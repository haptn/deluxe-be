package com.cax.demo.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Day extends Base {
	private String name;
	@ManyToMany(fetch = FetchType.EAGER)
	private Collection<Slot> slots = new ArrayList<>();

}
