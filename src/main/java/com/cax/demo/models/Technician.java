package com.cax.demo.models;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Technician extends Base {
	private String avatar;
	@Column(length = 7)
	private String status;
	@OneToMany(mappedBy="technician", fetch = FetchType.LAZY)
	private Collection<Appointment> appointment;
	@Column(length = 100)
	private String name;
	
}
