package com.cax.demo.test;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import com.cax.demo.models.Appointment;
import com.cax.demo.utils.DateTimeFormater;
import com.cax.demo.utils.SMTPMailSender;

import ch.qos.logback.classic.net.SMTPAppender;

public class Test {
	public static void main(String[] args) throws UnsupportedEncodingException, MessagingException  {
		Appointment app = new Appointment();
		app.setEmail("gauchocun@gmail.com");
		app.setTime(new Date());
		app.setName("Gia Hung");
		String[] strings = {"Nails","Hair"};
		List<String> services = Arrays.asList(strings);
			SMTPMailSender.sendMail(app, services);
		}
}
