package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.ServiceGroup;

@Repository
public interface ServiceGroupRepository extends JpaRepository<ServiceGroup, Long> {

}
