package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Account;
@Repository
public interface AccountRepository extends JpaRepository<Account, String> {
	Account findByUsername(String username);
	Account findByRefreshToken(String refreshToken);
}