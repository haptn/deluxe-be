package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Role;
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	Role findByName(String name);
}
