package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Day;
@Repository
public interface DayRepository extends JpaRepository<Day, Long>,JpaSpecificationExecutor<Day>{
	Day findByName(String name);
}
