package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Technician;
@Repository
public interface TechnicianRepository extends JpaRepository<Technician, Long> {
}
