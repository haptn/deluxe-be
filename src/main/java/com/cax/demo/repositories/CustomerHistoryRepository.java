package com.cax.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cax.demo.models.Customer;
import com.cax.demo.models.CustomerHistory;

public interface CustomerHistoryRepository extends JpaRepository<com.cax.demo.models.CustomerHistory, Long> {
	CustomerHistory findByCustomerAndStatus(Customer customer, String status);

	@Query(value = "SELECT h.customer_id FROM customer_history as h WHERE h.id = :id", nativeQuery = true)
	Long findCustomeridByHitoryId(long id);

	@Query(value = "SELECT c.customer_id  FROM customer_history as c WHERE c.status = :status ORDER BY c.checkout_time DESC LIMIT :limit ;", nativeQuery = true)
	List<Long> findTop5ByStatusOrderByCheckoutTimeDesc(String status, Integer limit);
}
