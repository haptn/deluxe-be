package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cax.demo.models.Flavor;
	
public interface FlavorRepository extends JpaRepository<Flavor, Long>{
	boolean existsFlavorByName(String name);
}
