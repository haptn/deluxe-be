package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Details;

public interface DetailRepository extends JpaRepository<Details, Long>{

}
