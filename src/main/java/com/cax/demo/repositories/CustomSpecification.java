package com.cax.demo.repositories;

import java.util.Date;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;
import com.cax.demo.models.Appointment;
import com.cax.demo.models.Appointment_;
import com.cax.demo.models.Details;
import com.cax.demo.models.Details_;
import com.cax.demo.models.ServiceOption;
import com.cax.demo.models.ServiceOption_;
import com.cax.demo.models.Services;
import com.cax.demo.models.Services_;
import com.cax.demo.models.Technician;
import com.cax.demo.models.Technician_;

public class CustomSpecification {

	public static Specification<?> likeKey(String key, String property) {
		if (!ObjectUtils.isEmpty(key)) {
			return (root, query, cb) -> {
				query.distinct(true);
				return cb.like(root.get(property), "%" + key + "%");
			};
		}
		return null;
	}

	public static Specification<?> equalKey(String key, String property) {
		if (!ObjectUtils.isEmpty(key)) {
			return (root, query, cb) -> {
				query.distinct(true);
				return cb.equal(root.get(property), key);
			};
		}
		return null;
	}

	public static Specification<Appointment> findAppointmentByServiceName(String key) {
		if (!ObjectUtils.isEmpty(key)) {
			return (root, query, cb) -> {
				Join<Appointment, Details> app_detail = root.join(Appointment_.DETAILS);
				Join<Details, ServiceOption> detail_option = app_detail.join(Details_.SERVICE_OPTION);
				Join<ServiceOption, Services> service_option = detail_option.join(ServiceOption_.SERVICE);
				query.distinct(true);
				return cb.like(service_option.get(Services_.NAME), "%" + key + "%");
			};
		}
		return null;
	}

	public static Specification<?> betweenDate(Date start, Date end, String property) {
		if (!ObjectUtils.isEmpty(start) && !ObjectUtils.isEmpty(end)) {
			return (root, query, cb) -> {
				query.distinct(true);
				return cb.between(root.get(property), start, end);
			};
		}
		return null;
	}

	public static Specification<Appointment> findAppoinmentByServiceIds(Long[] ids) {
		if (!ObjectUtils.isEmpty(ids)) {
			return (root, query, cb) -> {
				Join<Appointment, Details> app_detail = root.join(Appointment_.DETAILS);
				Join<Details, ServiceOption> detail_option = app_detail.join(Details_.SERVICE_OPTION);
				Join<ServiceOption, Services> service_option = detail_option.join(ServiceOption_.SERVICE);
				query.distinct(true);
				return cb.or(service_option.get(Services_.ID).in(ids));
			};
		}
		return null;
	}

	public static Specification<Appointment> findAppoinmentByTechIds(Long[] ids) {
		if (!ObjectUtils.isEmpty(ids)) {
			return (root, query, builder) -> {
				Join<Appointment, Technician> app_tech = root.join(Appointment_.TECHNICIAN);
				Predicate p = builder.conjunction();
				query.distinct(true);
				boolean flat = false;
				for (Long long1 : ids) {
					if (long1 == -1)
						flat = true;
				}
				Predicate normal = builder.or(app_tech.get(Services_.ID).in(ids));
				if (flat) {
					Predicate anyOne = builder.isNull(app_tech.get(Technician_.ID));
					p = builder.and(p, builder.or(anyOne, normal));
				} else
					p = builder.and(p, normal);
				return p;
			};
		}
		return null;
	}

	public static Specification<?> findInString(String[] strings, String property) {
		if (!ObjectUtils.isEmpty(strings)) {
			return (root, query, cb) -> {
				return cb.and(root.get(property).in(strings));
			};
		}
		return null;
	}

	public static Specification<?> isFalse(String property) {
		if (!ObjectUtils.isEmpty(property)) {
			return (root, query, cb) -> {
				return cb.isFalse(root.get(property));
			};
		}
		return null;
	}

}
