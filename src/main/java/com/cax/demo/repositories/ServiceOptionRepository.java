package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Flavor;
import com.cax.demo.models.ServiceOption;
import com.cax.demo.models.Services;
@Repository
public interface ServiceOptionRepository extends JpaRepository<ServiceOption, Long> {
	boolean existsServiceOptionByFlavorAndService(Flavor flavor,Services service);
}
