package com.cax.demo.repositories;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cax.demo.dtos.CustomerDTO;
import com.cax.demo.dtos.CustomerFilter;
import com.cax.demo.dtos.CustomerHistoryDTO;

@Repository
public class CustomCustomerRepo {

	@Autowired
	private EntityManager manager;

	private String QUERY_SQL = "select c.id, c.phone, count(CASE WHEN h.status = 'CHECK-OUT' THEN 1 END) as visited_count, \r\n"
			+ "sum(h.reward) as total_point,  max(h.checkin_time) as last_visited,  c.name, sum(h.discount) as discount, c.status as status, c.type as type, "
			+ "c.birth_date, c.address, c.email, c.spa "
			+ "from customer as c join customer_history as h on c.id = h.customer_id ";
	private String COUNT_SQL ="select count(DISTINCT(c.id)) from customer as c join customer_history as h on c.id = h.customer_id ";
	
	private String[] filter(String name, String phone, String status, Long id, String type, String spa) {
		String query = QUERY_SQL;
		String countQuery = COUNT_SQL;
		List<String> queryList = new ArrayList<>();
		if (name != null || phone != null || status != null || id != null || type != null || spa != null) {
			query = query + " where";
			countQuery = countQuery + "where ";
		}
		if (name != null && phone != null && status != null && id != null && type != null && spa != null) {
			queryList.add(" c.name LIKE '%" + name + "%' AND c.phone LIKE '%" + phone + "% AND c.status ='" + status
					+ "' AND c.id=" + id + " AND c.type=" + type + "' AND (c.spa='"+spa+"' OR c.spa is NULL) ");
//			query = query + " c.name LIKE '%" + name + "%' AND c.phone LIKE '%" + phone + "% AND c.status ='" +status+"' AND c.id="+id;
		} else {
			if (name != null) {
				queryList.add(" c.name LIKE '%" + name + "%'");				
			}
			if (phone != null) {
				queryList.add(" c.phone LIKE '%" + phone + "%'");
			}
			if (status != null) {
				queryList.add(" c.status ='" + status + "'");		
			}
			if (id != null) {
				queryList.add(" c.id ='" + id + "'");
			}
			if (type != null) {
				queryList.add(" c.type ='" + type + "'");
			}
			if (spa != null) {
				queryList.add(" (c.spa ='" + spa + "'  OR c.spa is NULL) ");
			}
		}
		if (!queryList.isEmpty()) {
			String condition = queryList.stream().map(n -> String.valueOf(n)).collect(Collectors.joining(" AND "));
			query = query + condition;
			countQuery = countQuery + condition;
		}
		query = query + " group by c.phone, c.name, c.id , c.status, c.type, c.address, c.email, c.birth_date, c.spa ";
		System.out.println(countQuery);
		String[] queries = {query, countQuery};
		return queries;
	}

	private String paging(Integer page, Integer limit, String sortBy, boolean asc) {
		String query = " order by " + sortBy + String.valueOf(asc ? " " : " desc ") + " limit " + limit + " OFFSET "
				+ page * limit;
		return query;
	}
	
	public int count(CustomerFilter filter) {
		String sql = filter(filter.getName(), filter.getPhone(), filter.getStatus(), filter.getId(), filter.getType(), filter.getSpa())[1];
		Query query = manager.createNativeQuery(sql);
		int count = 0;
		count = ((Number) query.getSingleResult()).intValue();
		return count;
	}

	public List<CustomerDTO> getAll(CustomerFilter filter) {
		String sql = filter(filter.getName(), filter.getPhone(), filter.getStatus(), filter.getId(), filter.getType(), filter.getSpa())[0]
				+ paging(filter.getPage(), filter.getLimit(), filter.getSortBy(),
						filter.getDirection().equalsIgnoreCase("asc"));
		Query query = manager.createNativeQuery(sql);
		List<CustomerDTO> dtos = new ArrayList<>();
		List<Object[]> result = query.getResultList();
		for (Object[] object : result) {
			BigInteger id = (BigInteger) object[0];
			String phone = (String) object[1];
			BigInteger visitedCount = (BigInteger) object[2];
			Double totalPoint = (Double) object[3];
			Date lastVisited = (Date) object[4];
			String fullName = (String) object[5];
			Double discount = (Double) object[6];
			String status = (String) object[7];
			String type = (String) object[8];
			Date brithdate = null;
			try {
				brithdate = (Date) object[9];
			} catch (Exception e) {

			}
			String address = (String) object[10];
			String email = (String) object[11];
			String spa = (String) object[12];
			dtos.add(CustomerDTO.builder().id(id.longValue()).phone(phone).visitCount(visitedCount.intValue())
					.totalPoint(totalPoint).fullName(fullName).lastVisited(lastVisited).discount(discount)
					.status(status).type(type).address(address).birthdate(brithdate).email(email).spa(spa).build());
		}
		return dtos;
	}

	public List<CustomerHistoryDTO> getAllHistoryByPhone(String phone) {
		String sql = "SELECT h.id, h.checkin_time, h.checkout_time, h.discount, h.price, h.request_time, h.reward, h.status, h.spa FROM customer_history as h"
				+ " join customer as c on h.customer_id = c.id where c.phone = :phone";
		Query query = manager.createNativeQuery(sql);
		query.setParameter("phone", phone);
		List<CustomerHistoryDTO> dtos = new ArrayList<>();
		List<Object[]> result = query.getResultList();
		for (Object[] object : result) {
			BigInteger id = (BigInteger) object[0];
			Date checkinTime = (Date) object[1];
			Date checkoutTime = (Date) object[2];
			Double discount = (Double) object[3];
			Double price = (Double) object[4];
			Date requestTime = (Date) object[5];
			Double reward = (Double) object[6];
			String status = (String) object[7];
			String spa = (String) object[8];
			CustomerHistoryDTO dto = new CustomerHistoryDTO(id.longValue(), checkinTime, checkoutTime, discount, price,
					requestTime, reward, status, spa);
			dtos.add(dto);
		}
		return dtos;
	}
}
