package com.cax.demo.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Appointment;
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long>,JpaSpecificationExecutor<Appointment> {
	long countByStatus(String status);
	
	@Query(value = "SELECT a.name FROM appointment as a JOIN promotion_history as p_h on a.id = p_h.appointment_id "
			+ "JOIN promotion_account p ON p_h.promotion_id = p.id  WHERE p.is_deleted =false and p_h.is_deleted = false and p.id = :id and a.name NOT IN(:name)", nativeQuery = true)
	Collection<String> findUsersForPromotionByPromotionId(long id, List<String> name);	
}
