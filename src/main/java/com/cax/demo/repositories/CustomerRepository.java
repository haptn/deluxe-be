package com.cax.demo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cax.demo.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	Optional<Customer> findByPhone(String phone);
	
	@Query(value = "SELECT * FROM customer as c WHERE EXISTS (SELECT * FROM customer_history as h"
			+ " WHERE c.id = h.customer_id and  (h.status = 'REQUEST'  or h.status = 'CHECK-IN') and c.phone = :phone"
			+ " and  (c.status = 'REQUEST' or c.status = 'CHECK-IN'))", nativeQuery = true)
	List<Customer> findRequestUnConfirmed(String phone);
}
