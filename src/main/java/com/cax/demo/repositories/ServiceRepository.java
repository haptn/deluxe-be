package com.cax.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cax.demo.models.Services;
@Repository
public interface ServiceRepository extends JpaRepository<Services, Long>,JpaSpecificationExecutor<Services>{

}
